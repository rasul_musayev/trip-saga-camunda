package az.ingress.bpm.orchestrator.trip;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import java.util.Random;

@Slf4j
public class BookFlight implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Booking flight {}", execution.getProcessInstanceId());

        Random random = new Random();
        if (random.nextBoolean()) {
            log.info("Successfully booked flight {}", execution.getProcessInstanceId());
        } else {
            log.info("Failed to book flight {}", execution.getProcessInstanceId());
            throw new RuntimeException("Failed to book fight");
        }
    }
}
