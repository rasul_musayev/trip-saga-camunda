package az.ingress.credit.application;

import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

@Slf4j
public class ValidateMkr implements JavaDelegate {
    @Override
    public void execute(DelegateExecution execution) throws Exception {
        log.info("Validating ASAN info for user {}", execution.getProcessInstanceId());
        log.info("Get customer name ,{}", execution.getVariable("lastName"));
    }
}
