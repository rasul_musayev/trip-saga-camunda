package az.ingress.saga.sagacamunda;

import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngines;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.runtime.ProcessInstanceWithVariables;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Map;

@SpringBootApplication
public class SagaCamundaApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SagaCamundaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = processEngine.getRuntimeService();


        ProcessInstanceWithVariables instance = runtimeService
                .createProcessInstanceByKey("CreditApplicationProcess")
                .setVariables(Map.of("firstName", "Resul"))
                .executeWithVariablesInReturn();
        System.out.println("Started new process with id " + instance.getProcessInstanceId());
 

    }
}
